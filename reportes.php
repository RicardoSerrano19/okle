<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">

    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">


    <title>Bienvenido</title>
</head>
<body>
    <header>
        <img src="img/logo.png" alt="">
        <div class="buscador">
            <input type="text" name="" id="" placeholder="Buscar información...">
            <i class="fas fa-search"></i>
        </div>
    </header>

    <main class="contenedor">

        <div class="informacionPrincipal">
            <div class="informacion flex-2r alinearArriba">
                <div class="botones flexCol">
                    <a href="#" id="reportes" class="btn btnMenu activo">Reportes</a>
                    <a href="estadisticas.php" id="estadisticas" class="btn btnMenu">Estadisticas</a>
                </div>
            </div><!--informacion-->
            <div class="imagen flex-10r flexCol">
                    <div class="btnNuevo">
                    <a href="" class="btn" id="agregarReporte">+ Nuevo</a>
                    </div>
                    <div class="reportes">

                        <div class="nuevoReporte block">
                            <h4>Nuevo reporte</h4>
                            <form action="" id="contacto" class="form">
                                <div class="campo">
                                    <label for="typeAggresion">Tipo de delito:</label>
                                    <select name="typeAggresion" id="typeAggresion">
                                        <option value="1">Acoso</option>
                                        <option value="2">Violencia Domestica</option>
                                        <option value="3">Violacion</option>
                                        <option value="4">Feminicidio</option>
                                        <option value="5">Asalto</option>
                                        <option value="6">Secuestro</option>
                                        <option value="7">Intengo de Secuestro</option>

                                    </select>
                                </div>
                                <div class="campo">
                                    <label for="fraccionamiento">Fraccionamiento</label>
                                    <input type="text" name="fraccionamiento" id="fraccionamiento">
                                </div>
                                <div class="campo">
                                    <label for="calle">Calle</label>
                                    <input type="text" name="calle" id="calle">
                                </div>
                                <div class="campo">
                                    <label for="hora">Hora</label>
                                    <input type="time" name="hora" id="hora">
                                </div>
                                <div class="campo">
                                    <label for="edad">Tu edad</label>
                                    <input type="number" name="edad" id="edad" min="0" max="100">
                                </div>
                                <div class="campo">
                                    <label for="alturaCriminal">Altura Criminal</label>
                                    <select name="alturaCriminal" id="alturaCriminal">
                                        <option value="1">Baja (1.50 - 1.65)</option>
                                        <option value="2">Media (1.66 - 1.80)</option>
                                        <option value="3">Alta (1.81 - 1.92)</option>
                                        <option value="4">Muy Alta (1.92 - Adelante)</option>
                                    </select>
                                </div>
                                <div class="campo">
                                    <label for="complexionCriminal">Complexion Criminal</label>
                                    <select name="complexionCriminal" id="complexionCriminal">
                                        <option value="1">Delgado</option>
                                        <option value="2">Medio</option>
                                        <option value="3">Gordo</option>
                                        <option value="4">Musculoso</option>
                                    </select>
                                </div>
                                <div class="campo">
                                    <label for="tezCriminal">Tez Criminal</label>
                                    <select name="tezCriminal" id="tezCriminal">
                                        <option value="1">Rubio</option>
                                        <option value="2">Blanco</option>
                                        <option value="3">Moreno</option>
                                        <option value="4">Negro</option>
                                    </select>
                                </div>
                                <div class="campo">
                                    <label for="rasgoAdicional">Rasgo adicional</label>
                                    <input type="text" name="rasgoAdicional" id="rasgoAdicional">
                                </div>
                                <div class="campo">
                                    <label for="descripcion">Descripcion Ataque</label>
                                    <textarea name="descripcion" id="descripcion" cols="10" rows="5"></textarea>
                                </div>

                                <div class="btnNuevo" id="nuevo">
                                    <a href="" id="estadisticas" class="btn" id="">Agregar</a>
                                </div>
                            </form>
                        </div><!--.nuevoReporte-->

                        <h3>Reportes Nuevos</h3>
                        
                        <?php
                        include 'includes/functions/funciones.php';
                        $contactos = obtenerContactos();
                        if($contactos ->num_rows){
                            foreach($contactos as $contacto){
                        ?>

                        <div class="reporte">
                                <div class="informacionReporte flex-10r">
                                    <p><?php echo utf8_decode($contacto['descripcion']) ?>
                                    </p>
                                    <p class="info">
                                        Denuncia Anonima - <?php echo $contacto['hora']?> - <?php 
                                        $agresionId = $contacto['tipoAgresion'];
                                        $agresion = obtenerContacto($agresionId);
                                        foreach($agresion as $agresiones){
                                            echo $agresiones['nombre'];
                                        }
                                        ?>
                                    </p>
                                </div>
                                <div class="destacarReporte flex-2r">
                                        <h4>Destacar</h4>
                                        <i class="fas fa-hands-helping"></i>
                                </div>
                            </div><!--.reporte-->
                    <?php
                            }
                        };
                    ?>

                    </div><!--.reportes-->
            </div>
        </div>
    </main>

    <footer>

    </footer>

</body>

<script src="js/main.js"></script>
</html>