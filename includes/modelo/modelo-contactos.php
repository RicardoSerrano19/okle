<?php
        require_once('../functions/conexionDB.php');
        $typeAggresion = $_POST['typeAggresion'];
        $fraccionamiento = filter_var($_POST['fraccionamiento'], FILTER_SANITIZE_STRING);
        $calle = filter_var($_POST['calle'], FILTER_SANITIZE_STRING);
        $hora = filter_var($_POST['hora'], FILTER_SANITIZE_STRING);
        $edad = filter_var($_POST['edad'], FILTER_SANITIZE_STRING);
        $alturaCriminal = filter_var($_POST['alturaCriminal'], FILTER_SANITIZE_STRING);
        $complexionCriminal = filter_var($_POST['complexionCriminal'], FILTER_SANITIZE_STRING);
        $tezCriminal = filter_var($_POST['tezCriminal'], FILTER_SANITIZE_STRING);
        $rasgoAdicional = filter_var($_POST['rasgoAdicional'], FILTER_SANITIZE_STRING);
        $descripcion = filter_var($_POST['descripcion'], FILTER_SANITIZE_STRING);
        $hoy = date("m/d/Y");
        $respuesta = "";

        try{
            $stmt = $conn -> prepare("INSERT INTO reportes (tipoAgresion,fraccionamiento,calle,hora,edad,alturaCriminal,complexionCriminal,tezCriminal,rasgoAdicional,fechaReporte,descripcion) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
            $stmt -> bind_param("issssssssss", $typeAggresion,$fraccionamiento,$calle,$hora,$edad,$alturaCriminal,$complexionCriminal,$tezCriminal,$rasgoAdicional,$hoy,$descripcion);
            $stmt -> execute();
            if($stmt-> affected_rows == 1){
                $respuesta = array(
                    'respuesta' => 'Correcto'
                );
            }
            $stmt -> close();
            $conn -> close();
   

        }catch(Exception $e){
            $respuesta = array(
                'error' => $e.getMessage()
            );
        }//Fin del try
        finally{
            echo json_encode($respuesta);
        }
?>