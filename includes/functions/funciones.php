<?php

    function obtenerContactos(){
        include 'conexionDB.php';

        try {
            return $conn ->query("SELECT * FROM reportes");
        } catch (Exception $e) {
            echo "Error!!" . $e->getMessage() . "<br>";
            return false;
        }
    }//Fin de la funcion obtenerContactos

    function obtenerContacto($id){
        include 'conexionDB.php';
        try {
            return $conn ->query("SELECT nombre FROM tiposAgresiones where idTipo = $id");
        } catch (Exception $e) {
            echo "Error!!" . $e->getMessage() . "<br>";
            return false;
        }
    }//Fin de la funcion obtenerContacto

    function getCountReportes(){
        include 'conexionDB.php';

        try {
            return $conn ->query("SELECT tipoAgresion, COUNT(tipoAgresion) contador FROM reportes GROUP BY tipoAgresion ORDER BY contador DESC");
        } catch (Exception $e) {
            echo "Error!!" . $e->getMessage() . "<br>";
            return false;
        }
    }//Fin de la funcion getCountReportes

    function getTezCriminal(){
        include 'conexionDB.php';

        try {
            return $conn ->query("SELECT tezCriminal, COUNT(tezCriminal) contador FROM reportes GROUP BY tezCriminal ORDER BY contador DESC");
        } catch (Exception $e) {
            echo "Error!!" . $e->getMessage() . "<br>";
            return false;
        }
    }//Fin de la funcion getTezCriminal

    function getColoniaConflictiva(){
        include 'conexionDB.php';

        try {
            return $conn ->query("SELECT fraccionamiento, COUNT(fraccionamiento) contador FROM reportes GROUP BY fraccionamiento ORDER BY contador DESC");
        } catch (Exception $e) {
            echo "Error!!" . $e->getMessage() . "<br>";
            return false;
        }
    }//Fin de la funcion getColoniaConflictiva

?>
