-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 05, 2019 at 06:17 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `safearea`
--

-- --------------------------------------------------------

--
-- Table structure for table `reportes`
--

CREATE TABLE `reportes` (
  `idReporte` int(11) NOT NULL,
  `tipoAgresion` int(11) NOT NULL,
  `fraccionamiento` varchar(30) NOT NULL,
  `calle` varchar(30) NOT NULL,
  `hora` varchar(20) NOT NULL,
  `edad` varchar(30) NOT NULL,
  `alturaCriminal` varchar(30) NOT NULL,
  `complexionCriminal` varchar(30) NOT NULL,
  `tezCriminal` varchar(30) NOT NULL,
  `rasgoAdicional` varchar(100) NOT NULL,
  `fechaReporte` varchar(30) NOT NULL,
  `descripcion` varchar(500) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reportes`
--

INSERT INTO `reportes` (`idReporte`, `tipoAgresion`, `fraccionamiento`, `calle`, `hora`, `edad`, `alturaCriminal`, `complexionCriminal`, `tezCriminal`, `rasgoAdicional`, `fechaReporte`, `descripcion`) VALUES
(10, 1, 'Ojocaliente l', 'Peñuelas', '17:34', '19', '1', '2', '2', 'NA', '15/10/2019', 'Un tipo me estuvo acosando en la parada del camión durante media hora'),
(11, 3, 'Estrella', 'Aries', '12:10', '25', '2', '3', '3', 'Ojos azules', '04/08/2019', 'Mi marido me golpeo por segunda vez'),
(12, 4, 'Bona Gens', 'pleyade', '20:00', '21', '1', '3', '4', 'Cabello largo', '12/09/2019', 'Un tipo en una van me metió en ella y me violo'),
(13, 7, 'Valle de los cactus', 'Anastacio Diaz', '06:40', '35', '2', '3', '4', '', '11/10/2019', 'Asesinato de mujer a balazos'),
(14, 3, 'Benito Palomino Dena', 'Pedro Rivas Cuéllar', '19:00', '65', '1', '1', '4', 'Pelo verde', '09/10/2019', 'Agresion de un sujeto bajo efecto de drogas hacia su madre'),
(15, 7, 'Centro', 'Galeana', '12:00', '40', '4', '1', '1', '', '29/09/2019', 'Presunto asesinato de abogada en un hotel'),
(16, 1, 'Morelos 2', 'Corregidora', '20:00', '16', '1', '2', '3', 'Tatuaje brazo derecho', '16/10/2019', 'Iba caminando rumbo a mi casa despues del entrenamiento, cuando un hombre me comenzo a seguir por mi trayecto para llegar a mi casa'),
(17, 1, 'Centro', 'Av.Lopez Mateos', '23:30', '19', '1', '1', '2', 'Calvo ', '07/09/2019', 'Al salir del estadio, estaba esperando un taxi para ir a mi casa, cuando me percate que un señor no muy viejo pero no tan joven me estaba observando,al caminar para alejarme de tal sujeto note que este me seguía cada vez más'),
(18, 1, 'Rodolfo Landeros', 'Rodolfo Landeros', '23:00', '23', '2', '2', '1', 'Tatuaje brazo izquierdo', '09/10/2019', 'Desconocido'),
(20, 5, 'Ojocaliente l', 'Peñuelas', '17:34', '19', '3', '2', '1', 'NA', '15/10/2019', 'Un tipo me estuvo acosando en la parada del camión durante media hora'),
(21, 3, 'Estrella', 'Aries', '12:10', '25', '1', '1', '3', 'Ojos azules', '04/08/2019', 'Mi marido me golpeo por segunda vez'),
(22, 4, 'Bona Gens', 'pleyade', '20:00', '21', '3', '3', '4', 'Cabello largo', '12/09/2019', 'Un tipo en una van me metió en ella y me violo'),
(23, 7, 'Valle de los cactus', 'Anastacio Diaz', '06:40', '35', '1', '2', '2', '', '11/10/2019', 'Asesinato de mujer a balazos'),
(24, 3, 'Benito Palomino Dena', 'Pedro Rivas Cuéllar', '19:00', '65', '1', '1', '1', 'Pelo verde', '09/10/2019', 'Agresion de un sujeto bajo efecto de drogas hacia su madre'),
(25, 7, 'Centro', 'Galeana', '12:00', '40', '3', '2', '2', '', '29/09/2019', 'Presunto asesinato de abogada en un hotel'),
(26, 1, 'Morelos 2', 'Corregidora', '20:00', '16', '4', '3', '3', 'Tatuaje brazo derecho', '16/10/2019', 'Iba caminando rumbo a mi casa despues del entrenamiento, cuando un hombre me comenzo a seguir por mi trayecto para llegar a mi casa'),
(27, 1, 'Centro', 'Av.Lopez Mateos', '23:30', '19', '1', '2', '4', 'Calvo ', '07/09/2019', 'Al salir del estadio, estaba esperando un taxi para ir a mi casa, cuando me percate que un señor no muy viejo pero no tan joven me estaba observando,al caminar para alejarme de tal sujeto note que este me seguía cada vez más'),
(28, 3, 'Rodolfo Landeros', 'Rodolfo Landeros', '23:00', '23', '1', '2', '3', 'Tatuaje brazo izquierdo', '09/10/2019', 'Desconocido'),
(29, 6, 'El dorado', 'Madrid', '15:24', '26', '3', '2', '2', 'Calvo con tatuaje', '23/07/2019', 'Un señor me arrebato mi bolsa cuando esperaba el autobus'),
(32, 7, 'Centro', 'Jefes', '00:31', '19', '1', '3', '1', 'Con perforacion', '11/13/2019', 'VI como un seÃ±or apuÃ±alo a una mujer hasta que esta ya no se pudo levantar y llego la ambulancia'),
(33, 5, 'Mexico', 'JEfes INsurgentes', '22:00', '22', '1', '1', '1', 'NInguno', '12/05/2019', 'EL taxista que me llevo fue un tanto grosero y al ultimo me asalto'),
(34, 3, 'Morelos', 'Sol', '19:09', '19', '1', '1', '4', 'Con tatuaje en la cara', '12/05/2019', 'Cuando llegue a mi casa en la madrugada unos tipos se hacercaron y me subieron en la camioneta, despues me drogaron y creeo abusaron de mi');

-- --------------------------------------------------------

--
-- Table structure for table `tiposAgresiones`
--

CREATE TABLE `tiposAgresiones` (
  `idTipo` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tiposAgresiones`
--

INSERT INTO `tiposAgresiones` (`idTipo`, `nombre`) VALUES
(1, 'Acoso'),
(2, 'Violencia Domestia'),
(3, 'Violacion'),
(4, 'Feminicidio'),
(5, 'Asalto'),
(6, 'Secuestro'),
(7, 'Intento de Secuestro');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `reportes`
--
ALTER TABLE `reportes`
  ADD PRIMARY KEY (`idReporte`),
  ADD KEY `tipoAgresion` (`tipoAgresion`);

--
-- Indexes for table `tiposAgresiones`
--
ALTER TABLE `tiposAgresiones`
  ADD PRIMARY KEY (`idTipo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `reportes`
--
ALTER TABLE `reportes`
  MODIFY `idReporte` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tiposAgresiones`
--
ALTER TABLE `tiposAgresiones`
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `reportes`
--
ALTER TABLE `reportes`
  ADD CONSTRAINT `reportes_ibfk_1` FOREIGN KEY (`tipoAgresion`) REFERENCES `tiposAgresiones` (`idTipo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
