<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">

    <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">


    <title>Bienvenido</title>
</head>
<body>
    <header>
        <img src="img/logo.png" alt="">
        <div class="buscador">
            <input type="text" name="" id="" placeholder="Buscar información...">
            <i class="fas fa-search"></i>
        </div>
    </header>

    <main class="contenedor">

        <div class="informacionPrincipal">
            <div class="informacion flex-2r alinearArriba">
                <div class="botones flexCol">
                    <a href="reportes.php" id="reportes" class="btn btnMenu">Reportes</a>
                    <a href="estadisticas.php" id="estadisticas" class="btn btnMenu activo">Estadisticas</a>
                </div>
            </div><!--informacion-->
            <div class="imagen flex-10r flexCol">
            <h3>Cantidad de reportes : 
            <?php
                    
                    include 'includes/functions/funciones.php';
                    $reportes = obtenerContactos();
                    echo "<span>";
                    echo $reportes ->num_rows;
                    echo "</span>";
                ?>

            </h3>

            <h3>Tipo de delito mas comun</h3>
            <div class="reporte row">

                        <?php
                        $reportes = getCountReportes();
                        if($reportes ->num_rows){
                            foreach($reportes as $reporte){

                                $agresionId = $reporte['tipoAgresion'];
                                        $agresion = obtenerContacto($agresionId);
                                        foreach($agresion as $agresiones){
                                            echo "<span>";
                                            echo $agresiones["nombre"] . " : ";
                                            echo $reporte["contador"]; 
                                            echo "</span>";
                                        }
                            }
                        }
                    ?>
            </div>
            <h3>Tez criminal mas comun</h3>
            <div class="reporte row">

                        <?php
                        $reporteTez = getTezCriminal();
                        if($reporteTez ->num_rows){
                            foreach($reporteTez as $reporteT){
                                echo "<span>";
                                if($reporteT["tezCriminal"] == 1){
                                    echo "Rubio";
                                }else if($reporteT["tezCriminal"] == 2){
                                    echo "Blanco";
                                }else if($reporteT["tezCriminal"] == 3){
                                    echo "Moreno";
                                }else if($reporteT["tezCriminal"] == 4){
                                    echo "Negro";
                                }
                                echo ": " .$reporteT["contador"]; 
                                echo "</span>";
                            }
                        }
                    ?>
            </div>
            

            <h3>Colonias con mas incidentes</h3>
            <div class="reporte row">

                        <?php
                        $colonias = getColoniaConflictiva();
                        if($colonias ->num_rows){
                            foreach($colonias as $colonia){
                                echo "<span>";
                                echo $colonia["fraccionamiento"]. " : "; 
                                echo $colonia["contador"]; 
                                echo "</span>";
                            }
                        }
                    ?>
            </div>
            

        </div>

    </main>

    <footer>

    </footer>

</body>

<script src="js/main.js"></script>
</html>
