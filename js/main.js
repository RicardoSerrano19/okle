const formularioContacto = document.querySelector('#contacto');
var nuevoReporte = document.getElementById("agregarReporte");
nuevoReporte.addEventListener('click',function(e){
    e.preventDefault();
   var plantillaReporte = document.querySelector(".nuevoReporte");
   if(plantillaReporte.classList.contains("block")){
       plantillaReporte.classList.remove("block");
    }else{
       plantillaReporte.classList.add("block");
   }
});

var nuevo = document.querySelector("#nuevo");
nuevo.addEventListener('click', function(e){
    e.preventDefault();
    leerFormulario(e);
});

function leerFormulario(e){
    e.preventDefault();

    //Leer datos de input
    const typeAggresion = document.querySelector('#typeAggresion').value;
    const fraccionamiento = document.querySelector('#fraccionamiento').value;
    const calle = document.querySelector('#calle').value;
    const hora = document.querySelector('#hora').value;
    const edad = document.querySelector('#edad').value;
    const alturaCriminal = document.querySelector('#alturaCriminal').value;
    const complexionCriminal = document.querySelector('#complexionCriminal').value;
    const tezCriminal = document.querySelector('#tezCriminal').value;
    const rasgoAdicional = document.querySelector('#rasgoAdicional').value;
    const descripcion = document.querySelector('#descripcion').value;


    if(typeAggresion === ""){
        alert("Error");
    }else{
        const infoContacto = new FormData();
        infoContacto.append('typeAggresion',typeAggresion);
        infoContacto.append('fraccionamiento',fraccionamiento);
        infoContacto.append('calle',calle);
        infoContacto.append('hora',hora);
        infoContacto.append('edad',edad);
        infoContacto.append('alturaCriminal',alturaCriminal);
        infoContacto.append('complexionCriminal',complexionCriminal);
        infoContacto.append('tezCriminal',tezCriminal);
        infoContacto.append('rasgoAdicional',rasgoAdicional);
        infoContacto.append('descripcion',descripcion);


        console.log(...infoContacto);

            //Crear un nuevo contacto
            insertarBD(infoContacto);
    

    }
}//Fin del metodo leerFormulario


function insertarBD(datos){
    //Llamar a AJAX
    //Crear el Objeto
    const xhr = new XMLHttpRequest();

    //Abrir la conexion
    xhr.open('POST','./includes/modelo/modelo-contactos.php',true);
    //Pasar los datos
    xhr.onload = function(){
            if(this.status===200){
                //Se obtiene la respuesta de JSON
                //const respuesta = JSON.parse(xhr.responseText);
                console.log(xhr.responseText);
                //numContactos.innerHTML = respuesta.numContactos;
                //Se crea la nueva fila para insertar
                /*const nuevoContacto = document.createElement('tr');
                nuevoContacto.innerHTML = `
                    <td>${respuesta.datos.nombre}</td>
                    <td>${respuesta.datos.empresa}</td>
                    <td>${respuesta.datos.telefono}</td>
                `;

                const contenedorAcciones = document.createElement('td')
                contenedorAcciones.classList.add('flex');
                //Se crea el btnEditar
                const iconoEditar = document.createElement('i');
                iconoEditar.classList.add('fas', 'fa-edit');
                
                const btnEditar = document.createElement('a');
                btnEditar.appendChild(iconoEditar);
                btnEditar.href = `editar.php?id=${respuesta.datos.idInsertado}`;
                btnEditar.classList.add('btnIcon', 'btnEditar');

                    //Agrega al padre
                    contenedorAcciones.appendChild(btnEditar);


                //Se crea el btnEliminar
                const iconoEliminar = document.createElement('i');
                iconoEliminar.classList.add('fas', 'fa-trash-alt');
                
                const btnEliminar = document.createElement('button');
                btnEliminar.appendChild(iconoEliminar);
                btnEliminar.setAttribute('data-id',respuesta.datos.idInsertado);
                btnEliminar.classList.add('btnIcon', 'btnBorrar');

                contenedorAcciones.appendChild(btnEliminar);

                  //Agrega al padre
                  contenedorAcciones.appendChild(btnEliminar);

                  nuevoContacto.appendChild(contenedorAcciones);

                listadoContactos.appendChild(nuevoContacto);


                mostrarNotificacion('Contacto Guardado','correcto');
                */
               borrarTextBox();

            }
    }
    //Enviar los datos
    xhr.send(datos);

}//fin de la funcion insertarBD


function borrarTextBox(){
    formularioContacto.reset();
    var plantillaReporte = document.querySelector(".nuevoReporte");
   plantillaReporte.classList.add("block");
}
